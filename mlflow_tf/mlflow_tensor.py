''' You always transpose the data when sending it to neural network, so array transposed so row x col 
becomes col x row, Code adopted from Kaggle Just for testing MLFlow workings'''
    

import mlflow
from mlflow import pyfunc
import pandas as pd
import tensorflow as tf
import mlflow.tensorflow
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
import numpy as np
import matplotlib.pyplot as plt
import math
import sys
import argparse
from argparse import RawTextHelpFormatter
from tensorflow.python.framework import ops

def tensorflow_shutup():
    """
    Make Tensorflow less verbose
    """
    try:
        # noinspection PyPackageRequirements
        import os
        from tensorflow import logging
        tf.compat.v1.logging.ERROR
        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

        # Monkey patching deprecation utils to shut it up! Maybe good idea to disable this once after upgrade
        # noinspection PyUnusedLocal
        def deprecated(date, instructions, warn_once=True):
            def deprecated_wrapper(func):
                return func
            return deprecated_wrapper

        from tensorflow.python.util import deprecation
        deprecation.deprecated = deprecated

    except ImportError:
        pass

tensorflow_shutup()

def create_placeholders(n_x, n_y):
    X = tf.placeholder(tf.float32, [n_x, None], name="X")
    Y = tf.placeholder(tf.float32, [n_y, None], name="Y")
    return X, Y

def initialize_parameters(num_input_features, num_output_features):  #here too!
    
    """
    Initializes parameters to build a neural network with tensorflow. The shapes are:
                        W1 : [num_hidden_layer, num_input_features]
                        b1 : [num_hidden_layer, 1]
                        W2 : [num_output_layer_1, num_hidden_layer]
                        b2 : [num_output_layer_1, 1]
                        W3 : [num_output_layer_2, num_output_layer_1]
                        b3 : [num_output_layer_2, 1]
    
    Returns:
    parameters -- a dictionary of tensors containing W1, b1, W2, b2, W3, b3
    """ 
    tf.set_random_seed(1)           
    W1 = tf.get_variable("W1", [10, num_input_features], initializer = tf.contrib.layers.xavier_initializer(seed=1))
    b1 = tf.get_variable("b1", [10, 1], initializer = tf.zeros_initializer())
    W2 = tf.get_variable("W2", [5, 10], initializer = tf.contrib.layers.xavier_initializer(seed=1))
    b2 = tf.get_variable("b2", [5, 1], initializer = tf.zeros_initializer())
    W3 = tf.get_variable("W3", [num_output_features, 5], initializer = tf.contrib.layers.xavier_initializer(seed=1)) #change label value
    b3 = tf.get_variable("b3", [num_output_features, 1], initializer = tf.zeros_initializer()) #change label value
    
    parameters = {"W1": W1,
                  "b1": b1,
                  "W2": W2,
                  "b2": b2,
                  "W3": W3,
                  "b3": b3}
    
    return parameters





def forward_propagation(X, parameters):
    """
    Implements the forward propagation for the model: 
    LINEAR -> RELU -> LINEAR -> RELU -> LINEAR -> SOFTMAX
    
    Arguments:
    X -- input dataset placeholder, of shape (input size, number of examples)
    parameters -- python dictionary containing your parameters 
    "W1", "b1", "W2", "b2", "W3", "b3"
    the shapes are given in initialize_parameters

    Returns:
    Z3 -- the output of the last LINEAR unit
    """
    # Retrieve the parameters from the dictionary "parameters" 
    W1 = parameters['W1']
    b1 = parameters['b1']
    W2 = parameters['W2']
    b2 = parameters['b2']
    W3 = parameters['W3']
    b3 = parameters['b3']
    
    ### START CODE HERE ### (approx. 5 lines)              # Numpy Equivalents:
    Z1 = tf.add(tf.matmul(W1, X), b1)                      # Z1 = np.dot(W1, X) + b1
    A1 = tf.nn.relu(Z1)                                    # A1 = relu(Z1)
    Z2 = tf.add(tf.matmul(W2, A1), b2)                     # Z2 = np.dot(W2, a1) + b2
    A2 = tf.nn.relu(Z2)                                    # A2 = relu(Z2)
    Z3 = tf.add(tf.matmul(W3, A2), b3)                     # Z3 = np.dot(W3,Z2) + b3
    ### END CODE HERE ###
    
    '''
     It is important to note that the forward propagation stops at z3. 
     The reason is that in tensorflow the last linear layer output is 
     given as input to the function computing the loss. 
     Therefore, you don't need a3!
    '''
    
    return Z3

def compute_cost(Z3, Y):
    """
    Computes the cost
    
    Arguments:
    Z3 -- output of forward propagation (output of the last LINEAR unit), of shape (3, number of examples)
    Y -- "true" labels vector placeholder, same shape as Z3
    
    Returns:
    cost - Tensor of the cost function
    """
    
    # to fit the tensorflow requirement for tf.nn.softmax_cross_entropy_with_logits(...,...)
    logits = tf.transpose(Z3)
    labels = tf.transpose(Y)
   
    #cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=labels))
    # The newer recommended function in Tensor flow
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=labels))
    return cost

def random_mini_batches(X, Y, mini_batch_size = 64, seed = 0):    
    """
    Creates a list of random minibatches from (X, Y)
    
    Arguments:
    X -- input data, of shape (input size, number of examples)
    Y -- true "label" vector (containing 0 if cat, 1 if non-cat), of shape (1, number of examples)
    mini_batch_size - size of the mini-batches, integer
    seed -- this is only for the purpose of grading, so that you're "random minibatches are the same as ours.
    
    Returns:
    mini_batches -- list of synchronous (mini_batch_X, mini_batch_Y)
    """
    m = X.shape[1]                  # number of training examples
    mini_batches = []
    np.random.seed(seed)
    
    # Step 1: Shuffle (X, Y)
    permutation = list(np.random.permutation(m))
    shuffled_X = X[:, permutation]
    shuffled_Y = Y[:, permutation].reshape((Y.shape[0],m))
    
     # Step 2: Partition (shuffled_X, shuffled_Y). Minus the end case.
    num_complete_minibatches = math.floor(m/mini_batch_size) # number of mini batches of size mini_batch_size in your partitionning
    for k in range(0, num_complete_minibatches):
        mini_batch_X = shuffled_X[:, k * mini_batch_size : k * mini_batch_size + mini_batch_size]
        mini_batch_Y = shuffled_Y[:, k * mini_batch_size : k * mini_batch_size + mini_batch_size]
        mini_batch = (mini_batch_X, mini_batch_Y)
        mini_batches.append(mini_batch)
    
    # Handling the end case (last mini-batch < mini_batch_size)
    if m % mini_batch_size != 0:
        mini_batch_X = shuffled_X[:, num_complete_minibatches * mini_batch_size : m]
        mini_batch_Y = shuffled_Y[:, num_complete_minibatches * mini_batch_size : m]
        mini_batch = (mini_batch_X, mini_batch_Y)
        mini_batches.append(mini_batch)
    
    return mini_batches

def model(X_train, Y_train, X_test, Y_test,num_epochs,minibatch_size ,num_input_features, num_output_features,learning_rate = 0.0001,
           print_cost = True):
    
    
    ops.reset_default_graph()                         # to be able to rerun the model without overwriting tf variables
    tf.set_random_seed(1)                             # to keep consistent results
    seed = 3                                          # to keep consistent results
    (n_x, m) = X_train.shape                          # (n_x: input size, m : number of examples in the train set)
    n_y = Y_train.shape[0]                            # n_y : output size
    costs = []                                        # To keep track of the cost
    
    # Create Placeholders of shape (n_x, n_y)
    X, Y = create_placeholders(n_x, n_y)

    # Initialize parameters
    parameters = initialize_parameters(num_input_features,num_output_features)
     # Forward propagation: Build the forward propagation in the tensorflow graph
    Z3 = forward_propagation(X, parameters)
    
    # Cost function: Add cost function to tensorflow graph
    cost = compute_cost(Z3, Y)
    
    # Backpropagation: Define the tensorflow optimizer. Use an AdamOptimizer.
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
    
    # Initialize all the variables
    init = tf.global_variables_initializer()

    # Start the session to compute the tensorflow graph
    with tf.Session() as sess:
        
        
        # Run the initialization
        sess.run(init)
        
        # Do the training loop
        for epoch in range(num_epochs):

            epoch_cost = 0.                       # Defines a cost related to an epoch
            num_minibatches = int(m / minibatch_size) # number of minibatches of size minibatch_size in the train set
            seed = seed + 1
            minibatches = random_mini_batches(X_train, Y_train, minibatch_size, seed)

            for minibatch in minibatches:

                # Select a minibatch
                (minibatch_X, minibatch_Y) = minibatch
                
                # IMPORTANT: The line that runs the graph on a minibatch.
                # Run the session to execute the "optimizer" and the "cost", the feedict should contain a minibatch for (X,Y).
                ### START CODE HERE ### (1 line)
                _ , minibatch_cost = sess.run([optimizer, cost], feed_dict={X: minibatch_X, Y: minibatch_Y})
                ### END CODE HERE ###
                epoch_cost += minibatch_cost / num_minibatches

            # Print the cost every epoch
            
            if print_cost == True and epoch % 100 == 0:
                print ("Cost after epoch %i: %f" % (epoch, epoch_cost))
            if print_cost == True and epoch % 5 == 0:
                costs.append(epoch_cost)
                
        # plot the cost
        plt.plot(np.squeeze(costs))
        plt.ylabel('cost')
        plt.xlabel('iterations (per tens)')
        plt.title("Learning rate =" + str(learning_rate))
        saved_fig = 'fig_sa.png'
        plt.savefig(saved_fig)
            
        
        #plt.show()
          # lets save the parameters in a variable
        parameters = sess.run(parameters)
        print("Parameters have been trained!")

        # Calculate the correct predictions
        correct_prediction = tf.equal(tf.argmax(Z3), tf.argmax(Y))

        # Calculate accuracy on the test set
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))

        print("Train Accuracy:", accuracy.eval({X: X_train, Y: Y_train}))
        print("Test Accuracy:", accuracy.eval({X: X_test, Y: Y_test}))
        test_acc= accuracy.eval({X: X_test, Y: Y_test})
# =============================================================================
#         saver = tf.train.Saver(max_to_keep=1)
# 
#         sa_tf = saver.save(sess,'./model', write_meta_graph=False)
#         with mlflow.start_run():
#             mlflow.log_artifact(sa_tf)
#         mlflow.end_run()
# # =============================================================================
# =============================================================================
         
# =============================================================================
        
        return parameters,test_acc,costs,saved_fig,minibatch_size,learning_rate,num_epochs

def process_data(data,total_data_size,test_size):
    training_set, test_set = train_test_split(data.iloc[:total_data_size,:].values,test_size=test_size, random_state = 12)

    X_train = training_set[:,:-1].transpose() 
    X_test = test_set[:,:-1].transpose() 

    #one hot encoding
    Y_train = to_categorical(training_set[:,-1])
    Y_train = Y_train[:,1:].transpose()

    Y_test = to_categorical(test_set[:,-1])
    Y_test = Y_test[:,1:].transpose()
    
    return X_train,X_test,Y_test,Y_train,training_set,test_set
    
    

def main_func():
    
    

    data = pd.read_csv('covtype.csv')
    
    minibatch_size = int(sys.argv[1])
    num_epochs = int(sys.argv[2])
    total_data_size = int(sys.argv[3]) 
    test_size = float(sys.argv[4])
   
  
# =============================================================================
#     minibatch_size = int(sys.argv[1]) if len(sys.argv) > 1 else 4
#     num_epochs = int(sys.argv[2]) if len(sys.argv) > 2 else 500
#     total_data_size = int(sys.argv[3]) if len(sys.argv) > 3 else 300
#     test_size = float(sys.argv[4]) if len(sys.argv) > 4 else 0.3
#     
# =============================================================================
    if(minibatch_size == 4 and num_epochs == 500 and total_data_size == 300 and test_size == 0.3):
        print(' This program can be ran by manually tuning the following Parameters \n Input the following parameters in a following way \n python file.py mini_batch_size no_of_epochs total_data_size test_size(in format %total_data_size) \n eg. python mlflow_tensor.py 10 1000 2000 0.4 \n Running on default params right now')
   
   
    X_train,X_test,Y_test,Y_train,training_set,test_set = process_data(data,total_data_size,test_size)

    ip_features, op_features = 54, np.max(training_set[:,-1]) #no. of classes
    
    
    tf.reset_default_graph()
        
    with tf.Session() as sess:
        X, Y = create_placeholders(ip_features, op_features)
        parameters = initialize_parameters(ip_features, op_features)
        Z3 = forward_propagation(X, parameters)
        cost = compute_cost(Z3, Y)
        print("cost = " + str(cost))


    
    parameters,fin_acc,tot_cost,sa_fig,mi_bat,lr,no_epo = model(X_train, Y_train, X_test, Y_test,num_epochs,minibatch_size,ip_features, op_features)
    
    with mlflow.start_run():
            mlflow.log_artifact(sa_fig)
            for value in range(len(tot_cost)):
               
               mlflow.log_metric('loss',tot_cost[value])
            
            mlflow.log_metric("Accuracy",fin_acc)
            mlflow.log_param("Input Features",ip_features)
            mlflow.log_param("Output Labels", op_features)
            mlflow.log_param("Training_Size",len(training_set))
            mlflow.log_param("Test_Size",len(test_set))
            mlflow.log_param("Learning_rate",lr)
            mlflow.log_param("mini_Batch_size",mi_bat)
            mlflow.log_param("No. of Epochs",no_epo)
    mlflow.end_run()

# =============================================================================
# 
# parser=argparse.ArgumentParser()
# parser.add_argument('This program can be ran by manually tuning the following Parameters',nargs = '+')
# parser.add_argument('Input the following parameters in a following way', nargs='+')
# parser.add_argument('python file.py mini_batch_size no_of_epochs total_data_size test_size(in format %total_data_size)', nargs='+')
# parser.add_argument('eg. python mlflow_tensor.py 10 1000 2000 0.4', nargs='+')
# parser.add_argument('Running on default params right now', nargs='+')
# 
# 
# =============================================================================
main_func()
# =============================================================================
# if len(sys.argv)==1:
#     parser.print_help(sys.stderr)
#     sys.exit(1)
#     main_func()
# else :  
#     main_func()
#     
# =============================================================================



